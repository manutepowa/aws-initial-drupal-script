#!/bin/bash

# Instalación de servicios después de instalar RUNCLOUD
# Ejecutar en la máquina como ROOT

echo -e "\e[31;43m***** Instalación DRUSH 8 global para D7 *****\e[0m"

git clone "https://github.com/drush-ops/drush.git" /usr/local/src/drush
cd /usr/local/src/drush
git checkout 8.x
ln -s /usr/local/src/drush/drush /usr/bin/drush
composer install



echo -e "\e[31;43m***** Instalación MCRYPT 7.2 *****\e[0m"
echo -e "\e[31;43m***** !!!! Para 7.3 necesita mcrypt-1.0.2 ¡¡¡¡ *****\e[0m"

apt update
apt -y install php7.2-dev
apt -y install gcc make autoconf libc-dev pkg-config
apt -y install libmcrypt-dev
printf "\n" | pecl install mcrypt-1.0.1
bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php/7.2/cli/conf.d/mcrypt.ini"
bash -c "echo extension=/usr/lib/php/20170718/mcrypt.so > /etc/php72rc/conf.d/mcrypt.ini"
systemctl restart php72rc-fpm.service




echo -e "\e[31;43m***** Activando Compresión GZIP APACHE y NGINX *****\e[0m"

echo "AddOutputFilterByType DEFLATE text/plain
AddOutputFilterByType DEFLATE text/html
AddOutputFilterByType DEFLATE text/xml
AddOutputFilterByType DEFLATE text/css
AddOutputFilterByType DEFLATE application/xml
AddOutputFilterByType DEFLATE application/xhtml+xml
AddOutputFilterByType DEFLATE application/rss+xml
AddOutputFilterByType DEFLATE application/javascript
AddOutputFilterByType DEFLATE application/x-javascript" > /etc/apache2-rc/conf.d/gzip.conf


echo "gzip on;
gzip_static on;
gzip_vary on;
gzip_disable 'MSIE [1-6]\.(?!.*SV1)';
gzip_types text/plain text/html text/css application/json application/x-javascript text/x$
gzip_comp_level 1;
gzip_buffers 16 8k;
gzip_min_length 10240;
gzip_proxied expired no-cache no-store private auth;" > /etc/nginx-rc/extra.d/gzip.conf



echo -e "\e[31;43m***** Aumentar memoria memcached *****\e[0m"

sed -i 's/-m 64/-m 1024/g' "/etc/memcached.conf"